import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';
import { ButtonModule } from '../../shared/button/button.module';
import { InputModule } from '../../shared/input/input.module';



@NgModule({
  declarations: [
    ProductComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    InputModule,
  ],
  exports: [
    ProductComponent
  ]
})
export class ProductsModule { }
